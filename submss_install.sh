#!/bin/bash

if [ "$#" -lt 1 ]; then
	echo "USAGE: $0 <version>" 
	exit
fi

RED='\033[0;31m'
BLUE='\033[0;34m'
NC='\033[0m' 
version=$1
steps=6

echo -e "${BLUE}[1/$steps]${NC} Purge previous images"
rm ../linux-image-*-submss_*-submss-*[^$version]*_amd64.deb &> /dev/null 
rm ../linux-headers-*-submss_*-submss-*[^$version]*_amd64.deb &> /dev/null 
rm ../linux-libc-dev_*-submss-${version}_amd64.deb
rm ../linux-*-submss_*-submss-${version}_amd64.changes
ssh -t root@tcp_server "cd /home/asadsa/tcp_testbed/kernel && rm images/*" &> /dev/null &
ssh -t root@tcp_client "cd /home/asadsa/tcp_testbed/kernel && rm images/*" &> /dev/null &

wait

echo -e "${BLUE}[2/$steps]${NC} Copy image $version to tcp_server"
#scp .version tcp_server:~/tcp_testbed/kernel
scp ../linux-image-*-submss_*-submss-${version}_amd64.deb tcp_server:~/tcp_testbed/kernel/images
scp ../linux-headers-*-submss_*-submss-${version}_amd64.deb tcp_server:~/tcp_testbed/kernel/images

echo -e "${BLUE}[3/$steps]${NC} Copy image $version to tcp_client"
#scp .version tcp_client:~/tcp_testbed/kernel
scp ../linux-image-*-submss_*-submss-${version}_amd64.deb tcp_client:~/tcp_testbed/kernel/images
scp ../linux-headers-*-submss_*-submss-${version}_amd64.deb tcp_client:~/tcp_testbed/kernel/images

echo -e "${BLUE}[4/$steps]${NC} Invoke installation of image $version on tcp_server"
ssh -t root@tcp_server "cd /home/asadsa/tcp_testbed/kernel && sudo ./install.sh" &> /dev/null &

echo -e "${BLUE}[5/$steps]${NC} Invoke installation of image $version on tcp_client"
ssh -t root@tcp_client "cd /home/asadsa/tcp_testbed/kernel && sudo ./install.sh" &> /dev/null &

wait

echo -e "${BLUE}[6/$steps]${NC} Waiting for tcp_server and tcp_client to be online again with the new image"

