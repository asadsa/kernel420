#!/bin/bash

cpu_cores=`getconf _NPROCESSORS_ONLN`

export KBUILD_BUILD_VERSION=0
make -j $((cpu_cores+1)) bindeb-pkg LOCALVERSION=-submss || { echo "Make failed" && exit 1; }

