#include <linux/module.h>
#include <linux/mm.h>
#include <net/tcp.h>
#include <linux/inet_diag.h>

#include "tcp_dctcp.h"

struct tcp_ls_aimd {
	struct dctcp dctcp;	/* must be first, dctcp_init writes here */
	u32 snd_cwnd_cnt;
	u32 frac_cwnd_clamp;
	u32 loss_frac_cwnd;
	u32 lg_k1;
	u32 cur_add_scale;
};

static unsigned int lg_k0 __read_mostly = 8; 
module_param(lg_k0, uint, 0644);
MODULE_PARM_DESC(lg_k0, "Logarithmic increase constant");

static unsigned int validate_pacing_timer __read_mostly = 1; 
module_param(validate_pacing_timer, uint, 0644);
MODULE_PARM_DESC(validate_pacing_timer, "Validate pacing timer in submss regime");

static unsigned int inc_add_scale __read_mostly = 8; 
module_param(inc_add_scale, uint, 0644);
MODULE_PARM_DESC(inc_add_scale, "Addtive Increase constant");

static unsigned int use_ls_aimd_reno __read_mostly; 
module_param(use_ls_aimd_reno, uint, 0644);
MODULE_PARM_DESC(use_ls_aimd_reno, "Use TCP LS-AIMD Reno even when ECN is present");

/* Prototypes */
static struct tcp_congestion_ops tcp_ls_aimd_dctcp;
static struct tcp_congestion_ops tcp_ls_aimd_reno;

static void tcp_ls_aimd_init(struct sock *sk)
{
	struct tcp_ls_aimd *ca = inet_csk_ca(sk);
	struct inet_connection_sock *icsk = inet_csk(sk);

	dctcp_init(sk);
	ca->snd_cwnd_cnt = 0;
	ca->loss_frac_cwnd = 0;
	ca->frac_cwnd_clamp = ~0U;
	ca->lg_k1 = lg_k0 + 1;
	ca->cur_add_scale = inc_add_scale;
	tcp_update_pacing_rate(sk);

	/* If DCTCP fall back to classic Reno use TCP LS-AIMD Reno instead */
	if (use_ls_aimd_reno || icsk->icsk_ca_ops != &tcp_ls_aimd_dctcp) {
		icsk->icsk_ca_ops = &tcp_ls_aimd_reno;
		/* FIXME: 4 should really be dctcp_shift_g */
		ca->dctcp.dctcp_alpha = DCTCP_MAX_ALPHA << 4;
		ca->dctcp.loss_cwnd = 0;
		pr_alert("DEBUG[%u]: TCP LS-AIMD Reno", ntohs(icsk->icsk_inet.inet_sport));
		return;
	}

	pr_alert("DEBUG[%u]: TCP LS-AIMD DCTCP", ntohs(icsk->icsk_inet.inet_sport));
}

static void tcp_ls_aimd_avoid_ai(struct sock *sk, u32 w, u32 acked)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct tcp_ls_aimd *ca = inet_csk_ca(sk);
	u32 delta;
	u32 add = tp->snd_add; 
	u32 scaled_mss = tp->mss_cache << ILOG2_SCALING_CONSTANT;

	/* If credits accumulated at a higher w, apply them gently now. */
	if (ca->snd_cwnd_cnt && ca->snd_cwnd_cnt >= w) {
		ca->snd_cwnd_cnt = 0;
		tp->snd_frac_cwnd += add;
	}

	ca->snd_cwnd_cnt += acked; 

	if (tcp_in_submss(tp)){
		u64 add_pace = (u64)add * scaled_mss * ca->snd_cwnd_cnt;
		u64 cwnd = (u64)tp->snd_cwnd * scaled_mss + tp->snd_frac_cwnd;
		ca->snd_cwnd_cnt = 0;

		add_pace = div64_u64(add_pace, cwnd);
		tp->snd_frac_cwnd += (u32)add_pace;
	} else if (ca->snd_cwnd_cnt >= w) {
		delta = ca->snd_cwnd_cnt / w; /*in segments of add bytes*/

		ca->snd_cwnd_cnt -= delta * w;
		tp->snd_frac_cwnd += delta * add; 
	}

	tp->snd_frac_cwnd = min(tp->snd_frac_cwnd, ca->frac_cwnd_clamp);

	if (tp->snd_frac_cwnd >= scaled_mss) { 
		delta = tp->snd_frac_cwnd / scaled_mss; /*in segments of SMSS bytes*/

		tp->snd_frac_cwnd -= delta * scaled_mss;
		tp->snd_cwnd = min(tp->snd_cwnd + delta, tp->snd_cwnd_clamp);
	}
}

static void tcp_ls_aimd_cong_avoid(struct sock *sk, u32 acked)
{
	struct tcp_sock *tp = tcp_sk(sk);

	if (!tcp_is_cwnd_limited(sk))
		return;

	/* In "safe" area, increase. */
	if (!tcp_in_submss(tp) && tcp_in_slow_start(tp)) {
		acked = tcp_slow_start(tp, acked);
		if (!acked)
			return;
	}
	/* In dangerous area, increase slowly. */
	tcp_ls_aimd_avoid_ai(sk, tp->snd_cwnd, acked);
}


static void __tcp_ls_aimd_set_state(struct sock *sk, u8 new_state)
{
	struct tcp_sock *tp = tcp_sk(sk);

	switch (new_state) {
	case TCP_CA_CWR:
	case TCP_CA_Recovery:
		tp->snd_cwnd = tp->snd_ssthresh;
		tp->snd_frac_cwnd = tp->snd_frac_ssthresh;

		tcp_check_submss(sk);
		break;
	}
}

static void tcp_ls_aimd_reno_set_state(struct sock *sk, u8 new_state)
{
	__tcp_ls_aimd_set_state(sk, new_state);
}

static void tcp_ls_aimd_dctcp_set_state(struct sock *sk, u8 new_state)
{
	__tcp_ls_aimd_set_state(sk, new_state);
	dctcp_state(sk, new_state);
}

/*     prev_pace_len
 * |---------x---------|
 * |-----------------------|
 *     new_pace_len
 *    tcp_wstamp += new - prev
 *     prev_pace_len
 * |---------x---------|
 * |---------------|
 *     new_pace_len
 *    tcp_wstamp -= prev - new
 */
static void tcp_ls_aimd_validate_pacing(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);
	u64 new_rate, prev_rate = (u64)sk->sk_pacing_rate;
	u64 new_pace_len_ns, prev_pace_len_ns = (u64)tp->pace_len_ns;
	u64 prev_skb_len;
	int status;

	if (unlikely(prev_rate == ~0UL || !prev_rate))
		return;

	if (!validate_pacing_timer || !tcp_in_submss(tp) || !tp->snd_credit) 
		return;

	/* New SRTT information tells us that a queue is building, or draining at 
	 * the bottleneck, so lets reinitate the pacing timer with the new
	 * intel we just gathered. We only needs to cancel the pacing
	 * timer and move tp->tcp_wstamp_ns accordingly. The timer rearm later.
	 */

	status = hrtimer_try_to_cancel(&tp->pacing_timer);

	if (status != 1)
		return;

	/* The timer has now been canceled, now adjust the refcount 
	 * accordingly.
	 */

	__sock_put(sk);

	/* Recalculate pacing rate with updated SRTT information to validate
	 * our previous calculation of pacing timer (must be done before we let the 
	 * CC adjust the cwnd).
	 */
	tcp_update_pacing_rate(sk);
	new_rate = (u64)sk->sk_pacing_rate;

	if (unlikely(new_rate == ~0UL || !new_rate))
		return;

	/* prev_skb_len is not exactly our previous len
	 * since the previous pace len took into account OS jitter, but
	 * this should be OK.
	 */

	prev_skb_len = (u64)prev_pace_len_ns * prev_rate;
	new_pace_len_ns = div64_ul((u64)prev_skb_len, new_rate);

	if (new_pace_len_ns > prev_pace_len_ns) {
		new_pace_len_ns -= (u64)prev_pace_len_ns;
		tp->tcp_wstamp_ns += new_pace_len_ns;
		tp->pace_len_ns += new_pace_len_ns;
	} else if (new_pace_len_ns < prev_pace_len_ns) {
		new_pace_len_ns = (u64)prev_pace_len_ns - new_pace_len_ns;
		tp->tcp_wstamp_ns -= new_pace_len_ns;
		tp->pace_len_ns -= new_pace_len_ns;
	}

	/* Wait until at least one segment has been transmitted before updating 
	 * the timeout again.
	 */
	tp->snd_credit = 0U;
}

static void tcp_ls_aimd_cong_control(struct sock *sk, const struct rate_sample *rs)
{
	struct tcp_ls_aimd *ca = inet_csk_ca(sk);
	u32 acked_sacked = rs->acked_sacked;

	tcp_ls_aimd_validate_pacing(sk);
	tcp_ls_aimd_cong_avoid(sk, acked_sacked);

	tcp_check_submss(sk);

	tcp_update_pacing_rate(sk);
}

static u32 tcp_ls_aimd_dctcp_ssthresh(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct tcp_ls_aimd *ca = inet_csk_ca(sk);
	u32 scaled_mss = tp->mss_cache << ILOG2_SCALING_CONSTANT;
	u32 alpha = ca->dctcp.dctcp_alpha; 
	u64 cwnd = (u64)tp->snd_cwnd * scaled_mss + tp->snd_frac_cwnd;
	/* FIXME: +4 should really be +dctcp_shift_g */
	u64 cwnd_delta = (((u64)cwnd * alpha) >> (DCTCP_ALPHA_WIDTH + 5U)); 
	u64 cwnd_cnt = (u64)cwnd - cwnd_delta;
	u32 frac_cwnd_cnt = do_div(cwnd_cnt, scaled_mss);
	
	ca->dctcp.loss_cwnd = tp->snd_cwnd;
	ca->loss_frac_cwnd = tp->snd_frac_cwnd;

	tp->snd_frac_ssthresh = cwnd_cnt ? frac_cwnd_cnt : 
		max(frac_cwnd_cnt, 2U << ILOG2_SCALING_CONSTANT);

	return (u32)cwnd_cnt;
}

static u32 tcp_ls_aimd_reno_ssthresh(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct tcp_ls_aimd *ca = inet_csk_ca(sk);
	u32 scaled_mss = tp->mss_cache << ILOG2_SCALING_CONSTANT;
	u64 cwnd_cnt = ((u64)tp->snd_cwnd * scaled_mss + tp->snd_frac_cwnd) >> 1;
	u32 frac_cwnd_cnt;

	frac_cwnd_cnt = do_div(cwnd_cnt, scaled_mss);
	
	ca->dctcp.loss_cwnd = tp->snd_cwnd;
	ca->loss_frac_cwnd = tp->snd_frac_cwnd;

	tp->snd_frac_ssthresh = cwnd_cnt ? frac_cwnd_cnt : 
		max(frac_cwnd_cnt, 2U << ILOG2_SCALING_CONSTANT);

	return (u32)cwnd_cnt;
}

static u32 tcp_ls_aimd_cwnd_undo(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct tcp_ls_aimd *ca = inet_csk_ca(sk);
	int delta = tp->snd_cwnd - ca->dctcp.loss_cwnd;

	if (delta > 0)
		return tp->snd_cwnd;

	tp->snd_frac_cwnd = delta < 0 ? ca->loss_frac_cwnd :
		max(tp->snd_frac_cwnd, ca->loss_frac_cwnd);

	return ca->dctcp.loss_cwnd;
}

/*
 * Add <= ssthresh, MUST be less than one decrease
 */
static u32 tcp_ls_aimd_add(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct tcp_ls_aimd *ca = inet_csk_ca(sk);
	u32 scaled_mss = tp->mss_cache << ILOG2_SCALING_CONSTANT;
	u64 ssthresh = (u64)tp->snd_ssthresh * scaled_mss + tp->snd_frac_ssthresh;
	u64 add;

	ssthresh += 1ULL << (ca->lg_k1 + ILOG2_SCALING_CONSTANT);
	add = (u64)ilog2_scaled_accurate(ssthresh);
	add -= ca->lg_k1 << ILOG2_SCALING_CONSTANT;
	add <<= lg_k0;

	/* Scale Add: 0...1 */
	add >>= (8U - min(ca->cur_add_scale, 8U));
	return max_t(u32, add, (1U << ILOG2_SCALING_CONSTANT));
}

static struct tcp_congestion_ops tcp_ls_aimd_dctcp __read_mostly = {
	.init			= tcp_ls_aimd_init,
	.in_ack_event   = dctcp_update_alpha,
	.cwnd_event		= dctcp_cwnd_event,
	.ssthresh		= tcp_ls_aimd_dctcp_ssthresh,
	.set_state		= tcp_ls_aimd_dctcp_set_state,
	.undo_cwnd		= tcp_ls_aimd_cwnd_undo,
	.current_add	= tcp_ls_aimd_add,
	.cong_control	= tcp_ls_aimd_cong_control,
	.flags			= TCP_CONG_NEEDS_ECN,
	.owner			= THIS_MODULE,
	.name			= "ls-aimd-dctcp",
};

static struct tcp_congestion_ops tcp_ls_aimd_reno __read_mostly = {
	.ssthresh		= tcp_ls_aimd_reno_ssthresh,
	.set_state		= tcp_ls_aimd_reno_set_state,
	.undo_cwnd		= tcp_ls_aimd_cwnd_undo,
	.current_add	= tcp_ls_aimd_add,
	.cong_control	= tcp_ls_aimd_cong_control,
	.flags			= TCP_CONG_NEEDS_ECN,
	.owner			= THIS_MODULE,
	.name			= "ls-aimd-reno",
};

static int __init tcp_ls_aimd_register(void)
{
	BUILD_BUG_ON(sizeof(struct tcp_ls_aimd) > ICSK_CA_PRIV_SIZE);
	return tcp_register_congestion_control(&tcp_ls_aimd_dctcp);
}

static void __exit tcp_ls_aimd_unregister(void)
{
	tcp_unregister_congestion_control(&tcp_ls_aimd_dctcp);
}

module_init(tcp_ls_aimd_register);
module_exit(tcp_ls_aimd_unregister);

MODULE_AUTHOR("Asad Sajjad Ahmed <asadsa@ifi.uio.no>");
MODULE_AUTHOR("Bob Briscoe <research@bobbriscoe.net>");

MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("LS-AIMD DCTCP/Reno");
